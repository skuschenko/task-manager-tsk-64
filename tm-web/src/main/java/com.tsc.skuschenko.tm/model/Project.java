package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Project {

    @Nullable
    private Date created = new Date();

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Nullable
    private String description = "";

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String name = "";

    @Nullable
    private Status status = Status.NOT_STARTED;

    public Project(@Nullable String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}

package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProjectListController {

    @Autowired
    @NotNull
    private ProjectRepository projectRepository;

    @GetMapping("/projects")
    @NotNull
    public ModelAndView index() {
        return new ModelAndView(
                "project-list", "projects",
                projectRepository.findAll()
        );
    }

}

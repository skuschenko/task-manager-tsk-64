package com.tsc.skuschenko.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.tsc.skuschenko.tm")
public class ApplicationConfiguration {

}

package com.tsc.skuschenko.tm.listener.system;

import com.tsc.skuschenko.tm.api.service.IListenerService;
import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;

@Component
public final class HelpShowListener extends AbstractListener {

    private static final String ARGUMENT = "-h";

    private static final String DESCRIPTION = "help";

    private static final String NAME = "help";

    @Autowired
    private IListenerService commandService;

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@helpShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showOperationInfo(NAME);
        @NotNull final Collection<AbstractListener> commands =
                commandService.getListeners();
        commands.stream().filter(item -> Optional.ofNullable(item).isPresent())
                .forEach(item -> {
                    String result = "";
                    if (Optional.ofNullable(item.name()).isPresent()) {
                        result += item.name();
                    }
                    if (Optional.ofNullable(item.arg()).isPresent()) {
                        result += " [" + item.arg() + "]";
                    }
                    if (Optional.ofNullable(item.description()).isPresent()) {
                        result += " - " + item.description();
                    }
                    System.out.println(result);
                });
    }

    @Override
    public String name() {
        return NAME;
    }

}

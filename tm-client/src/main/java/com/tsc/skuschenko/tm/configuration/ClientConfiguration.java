package com.tsc.skuschenko.tm.configuration;

import com.tsc.skuschenko.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.tsc.skuschenko.tm")
public class ClientConfiguration {

    @Bean
    @NotNull
    public DataEndpoint dataEndpoint() {
        @NotNull final DataEndpointService dataEndpointService =
                new DataEndpointService();
        return dataEndpointService.getDataEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint() {
        @NotNull final ProjectEndpointService projectEndpointService =
                new ProjectEndpointService();
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public SessionEndpoint sessionEndpoint(
    ) {
        @NotNull final SessionEndpointService sessionEndpointService =
                new SessionEndpointService();
        return sessionEndpointService.getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint() {
        @NotNull final TaskEndpointService taskEndpointService =
                new TaskEndpointService();
        return taskEndpointService.getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint() {
        @NotNull final UserEndpointService userEndpointService =
                new UserEndpointService();
        return userEndpointService.getUserEndpointPort();
    }

}
